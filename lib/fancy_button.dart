import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

//https://www.youtube.com/watch?v=W1pNjxmNHNQ&list=PLjxrf2q8roU2HdJQDjJzOeO6J3FoFLWr2&index=2&ab_channel=GoogleDevelopers

class FancyButton extends StatelessWidget {
  FancyButton({@required this.onPressed});

  final GestureTapCallback onPressed;

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
        fillColor: Colors.deepOrange,
        splashColor: Colors.orange,
        child: Padding(
          padding: const EdgeInsets.symmetric(
              vertical: 8.0,
              horizontal: 20.0,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: const <Widget> [
              RotatedBox(
                quarterTurns: 1,
                child: Icon(
                  Icons.explore,
                  color: Colors.amber,
                ),
              ),
              SizedBox(width: 8.0),
              Text(
                  "INCREMENT",
                  style: TextStyle(
                    color: Colors.white
                )
              ),
            ],
          ),
        ),
        onPressed: onPressed,
      shape: const StadiumBorder(),
    );
  }
}
